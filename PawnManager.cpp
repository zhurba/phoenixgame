#include "PawnManager.h"
#include "MapManager.h"

PawnManager::~PawnManager()
{
}

PawnManager& PawnManager::Instance()
{
    static PawnManager s_instance;
    return s_instance;
}

void PawnManager::Draw() const
{
    for (auto const& pawn : m_pawns)
    {
        pawn->Draw();
    }
}

void PawnManager::AddPawn()
{
    m_pawns.emplace_back(std::make_shared<Pawn>());
    m_pawns.back()->SetPos(0, 0);
    m_pawns.back()->SetSize(1, 1);
    m_pawns.back()->SetColor(1.f, 0.f, 0.f);
    m_pawns.back()->Initialize();
    MapManager::Instance().PlacePawn(m_pawns.back());
}

void PawnManager::DeletePawn()
{
    // todo
    // (including UnregisterReceiver)
}

void PawnManager::SelectPawn(int x, int y)
{
    for (auto const& pawn : m_pawns)
    {
        if (pawn->GetPosX() == x && pawn->GetPosY() == y)
        {
            m_activePawn = pawn;
            return;
        }
    }

    m_activePawn = nullptr;
}

void PawnManager::SetAimToActivePawn(int const aimPosX, int const aimPosY)
{
    if (m_activePawn)
    {
        MapManager::Instance().FindPath(m_activePawn, aimPosX, aimPosY);
        m_activePawn->SetAimPos(aimPosX, aimPosY);
    }
}

void PawnManager::MovePawns() const
{
    for (auto& pawn : m_pawns)
    {
        pawn->Move();
    }
}

PawnManager::PawnManager()
    : m_pawns()
    , m_activePawn(nullptr)
{
}
