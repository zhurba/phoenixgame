#include "MapManager.h"
#include "Constants.h"
#include "Pawn.h"
#include "TPathfinder.h"

MapManager::~MapManager()
{
}

MapManager& MapManager::Instance()
{
    static MapManager instance;
    return instance;
}

//void MapManager::Initialize()
//{
    //m_field = std::make_shared<TField<std::weak_ptr<Pawn>>>(MAP_WIDTH, MAP_HEIGHT, std::weak_ptr<Pawn>());
//}

//std::shared_ptr<TField<std::weak_ptr<Pawn>>> MapManager::GetField() const
//{
//    return m_field;
//}

void MapManager::PlacePawn(std::shared_ptr<Pawn> const& pawn)
{
    pawn->m_senderPosSet.RegisterReceiver(MapManager::Instance().m_receiverPawnPosSet);
    TFieldPoint<std::weak_ptr<Pawn>>* fieldPoint = m_field.GetPoint(pawn->GetPosX(), pawn->GetPosY());
    if (fieldPoint)
    {
        bool occupied = (pawn != nullptr);
        fieldPoint->SetValue(pawn, occupied);
    }
}

void MapManager::FindPath(std::shared_ptr<Pawn>& pawn, int const endPosX, int const endPosY)
{
    if (!pawn)
    {
        return;
    }

    std::list<TFieldPoint<std::weak_ptr<Pawn>>*> pointPath;
    TPathfinder<std::weak_ptr<Pawn>>::FindPath(m_field, pointPath, pawn->GetPosX(), pawn->GetPosY(), endPosX, endPosY);

    std::list<std::array<int, 2>> path;
    for (auto point : pointPath)
    {
        path.push_back({ point->m_x, point->m_y });
    }
    pawn->SetPath(path);
}

void MapManager::UpdatePawnPosition(Pawn const* pawn, int oldX, int oldY)
{
    if (!pawn)
    {
        return;
    }

    int const newX = pawn->GetPosX();
    int const newY = pawn->GetPosY();
    if (oldX == newX && oldY == newY)
    {
        return;
    }

    if (!m_field.HasPoint(oldX, oldY))
    {
        printf("Error: the pawn was outside the map!\n");
        return;
    }

    TFieldPoint<std::weak_ptr<Pawn>>* oldFieldPoint = m_field.GetPoint(oldX, oldY);
    std::weak_ptr<Pawn> wpPawn = oldFieldPoint->GetValue();
    oldFieldPoint->SetValue(std::weak_ptr<Pawn>(), false);
    //oldFieldPoint->m_val.reset();

    if (!m_field.HasPoint(newX, newY))
    {
        printf("Error: the pawn is outside the map!\n");
        return;
    }

    TFieldPoint<std::weak_ptr<Pawn>>* newFieldPoint = m_field.GetPoint(newX, newY);
    newFieldPoint->SetValue(wpPawn, true);
}

void MapManager::OnPawnPosSet(SenderParamsBase const* paramsBase)
{
    if (auto params = dynamic_cast<SenderParams<Pawn*, int, int> const*>(paramsBase))
    {
        Pawn const* pawn = params->m_arg1;
        int const oldX = params->m_arg2;
        int const oldY = params->m_arg3;
        UpdatePawnPosition(pawn, oldX, oldY);
    }
}

MapManager::MapManager()
    : m_receiverPawnPosSet(nullptr)
    , m_field(MAP_WIDTH, MAP_HEIGHT, std::weak_ptr<Pawn>())
{
    m_receiverPawnPosSet = std::make_shared<Receiver<MapManager>>(this, &MapManager::OnPawnPosSet);
}
