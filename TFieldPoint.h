#pragma once

template<typename ValueType>
class TFieldPoint
{
public:
    struct FPCompareByLength
    {
        bool operator()(const TFieldPoint<ValueType>* p1, const TFieldPoint<ValueType>* p2) const
        {
            return p1->m_pathLength + p1->m_estLength < p2->m_pathLength + p2->m_estLength;
        }
    };

    TFieldPoint(const ValueType val, const int valX, const int valY, const int valPathLength = 0, double valEstLength = 0.0)
        : m_x(valX)
        , m_y(valY)
        , m_pathLength(valPathLength)
        , m_estLength(valEstLength)
        , m_prev(nullptr)
        , m_val(val)
        , m_occupied(false)
    {
        m_prev = this;
    }

    bool operator==(const TFieldPoint& other) const
    {
        return m_x == other.m_x && m_y == other.m_y;
    }

    bool IsOccupied() const
    {
        return m_occupied;
    }

    ValueType GetValue() const
    {
        return m_val;
    }

    void SetValue(ValueType val, bool occupied)
    {
        m_val = val;
        m_occupied = occupied;
    }

    void Init()
    {
        m_pathLength = 0;
        m_estLength = 0.0;
        m_prev = this;
    }

    int m_x;
    int m_y;
    int m_pathLength;
    double m_estLength;
    TFieldPoint* m_prev;

private:
    ValueType m_val;
    bool m_occupied;
};
