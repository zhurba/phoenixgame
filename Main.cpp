#include <GL/freeglut.h>
#include "Constants.h"
#include "InputController.h"

void Initialize()
{
    InputController::Instance().Initialize();
}

void mainDraw()
{
    InputController::Instance().OnDraw();
}

void mainChangeSize(int width, int height)
{
    //Controller::Instance().ChangeSize(width, height);
}

void mainIdle()
{
    InputController::Instance().OnUpdate();
    glutPostRedisplay();
}

void mainOnKeyPress(unsigned char key, int x, int y)
{
    InputController::Instance().OnKeyPress(key, x, y);
}

void mainOnSpecialKeyPress(int key, int x, int y)
{
    //Controller::Instance().OnSpecialKeyPress(key, x, y);
}

void mainOnMousePress(int button, int state, int x, int y)
{
    InputController::Instance().OnMousePress(button, state, x, y);
}

void mainOnMousePressAndMove(int x, int y)
{
    //Controller::Instance().OnMousePressAndMove(x, y);
}

void mainOnMouseMove(int x, int y)
{
    //Controller::Instance().OnMouseMove(x, y);
}

int main(int argc, char** argv)
{
    //initialize:
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
    glutInitWindowSize(SCREEN_WIDTH, SCREEN_HEIGHT);
    glutInitWindowPosition(SCREEN_TOP, SCREEN_LEFT);
    glutCreateWindow("Phoenix Game");

    //register:
    glutDisplayFunc(mainDraw); //on display, paint, refresh
    glutReshapeFunc(mainChangeSize); //on resize window
    glutIdleFunc(mainIdle); //on wait

    //key events:
    glutKeyboardFunc(mainOnKeyPress);
    glutSpecialFunc(mainOnSpecialKeyPress);

    //mouse events:
    glutMouseFunc(mainOnMousePress);
    glutMotionFunc(mainOnMousePressAndMove);
    glutPassiveMotionFunc(mainOnMouseMove);

    //Viewer viewer(SCREEN_WIDTH, SCREEN_HEIGHT);
    //Controller::Instance().SetViewer(viewer);
    //THE_APP->AddUnit();
    //viewer.SetView();

    Initialize();

    //main loop:
    glutMainLoop();
    return 0;
}