#pragma once
class BitStream
{
public:
    BitStream(const char* buffer, const unsigned int length);
    ~BitStream();

    bool IsEmpty();
    bool IsEof();
    void PopBitsRight(unsigned int number, unsigned int& popValue);
    unsigned int PopBitsRight(unsigned int number);

private:
    char* GetBuffer();

private:
    char* m_buffer;
    unsigned int m_length;
    unsigned int m_pos;
    char m_bitPos;
};

