#include "Pawn.h"
#include "Constants.h"
#include "MapManager.h"
#include "ImageLoader.h"

const GLfloat g_maxMoveRate = 1.f;
const GLfloat g_moveRateStep = 0.001f;

Pawn::Pawn()
    : m_senderPosSet()
    , m_textureId(0)
    , m_color{ 1.f, 0.f, 0.f }
    , m_size{ 1, 1 }
    , m_pos {0, 0}
    , m_nextPos{0, 0}
    , m_aimPos {0, 0}
    , m_moveRate(0.f)
    , m_path()
{
}

Pawn::~Pawn()
{
}

void Pawn::Initialize()
{
    m_nextPos = m_pos;
    m_aimPos = m_pos;

    std::string textureFileName = "D:/CPP/PhoenixGame/Debug/Green.png";
    std::unique_ptr<Image> upImage = nullptr;
    if (textureFileName.find(".png") != std::string::npos)
    {
        upImage = ImageLoader::LoadImagePNG(textureFileName);
    }
    else if (textureFileName.find(".bmp") != std::string::npos)
    {
        upImage = ImageLoader::LoadImageBMP(textureFileName);
    }

    if (upImage == nullptr)
    {
        return;
    }

    glGenTextures(1, &m_textureId);
    glBindTexture(GL_TEXTURE_2D, m_textureId);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexImage2D(GL_TEXTURE_2D, 0, GLUT_RGBA, upImage->GetWidth(), upImage->GetHeight(), 0, GLUT_RGBA, GL_UNSIGNED_BYTE, (unsigned char*)upImage->GetData());
    //glGenerateMipmap(GL_TEXTURE_2D);

    // free image data

    glBindTexture(GL_TEXTURE_2D, 0);
}

void Pawn::Draw() const
{
    GLfloat left = (m_pos[0] * (1 - m_moveRate) + m_nextPos[0] * m_moveRate) * CELL_WIDTH;
    GLfloat top = (m_pos[1] * (1 - m_moveRate) + m_nextPos[1] * m_moveRate) * CELL_HEIGHT;
    GLfloat width = m_size[0] * CELL_WIDTH;
    GLfloat height = m_size[1] * CELL_HEIGHT;

    glBindTexture(GL_TEXTURE_2D, m_textureId);
    glEnable(GL_TEXTURE_2D);

    //glColor3fv(m_color.data());
    glBegin(GL_QUADS);
    glTexCoord2i(0, 1);
    glVertex2f(left, top);
    glTexCoord2i(1, 1);
    glVertex2f(left + width, top);
    glTexCoord2i(1, 0);
    glVertex2f(left + width, top + height);
    glTexCoord2i(0, 0);
    glVertex2f(left, top + height);
    glEnd();

    glFlush();

    glDisable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, 0);
}

void Pawn::SetColor(GLfloat r, GLfloat g, GLfloat b)
{
    m_color[0] = r;
    m_color[1] = g;
    m_color[2] = b;
}

void Pawn::SetSize(int w, int h)
{
    m_size[0] = w;
    m_size[1] = h;
}

void Pawn::SetPos(int x, int y)
{
    int const oldX = m_pos[0];
    int const oldY = m_pos[1];
    m_pos[0] = x;
    m_pos[1] = y;

    SenderParams<Pawn*, int, int> params(this, oldX, oldY);
    m_senderPosSet.Notify(&params);
}

int Pawn::GetPosX() const
{
    return m_pos[0];
}

int Pawn::GetPosY() const
{
    return m_pos[1];
}

void Pawn::SetAimPos(int x, int y)
{
    m_aimPos[0] = x;
    m_aimPos[1] = y;
}

int Pawn::GetAimPosX() const
{
    return m_aimPos[0];
}

int Pawn::GetAimPosY() const
{
    return m_aimPos[1];
}

void Pawn::Move()
{
    if (m_pos[0] == m_aimPos[0] && m_pos[1] == m_aimPos[1])
    {
        return;
    }

    if (m_pos[0] == m_nextPos[0] && m_pos[1] == m_nextPos[1])
    {
        NextStep();
    }

    m_moveRate += g_moveRateStep;
    if (m_moveRate >= g_maxMoveRate)
    {
        m_moveRate = 0.f;
        NextStep();
    }

}

void Pawn::NextStep()
{
    SetPos(m_nextPos[0], m_nextPos[1]);
    if (!m_path.empty())
    {
        m_nextPos[0] = m_path.front()[0];
        m_nextPos[1] = m_path.front()[1];
        m_path.pop_front();
    }
    /*for (int dir = 0; dir < 2; ++dir)
    {
        if (m_nextPos[dir] < m_aimPos[dir])
        {
            ++m_nextPos[dir];
        }
        else if (m_nextPos[dir] > m_aimPos[dir])
        {
            --m_nextPos[dir];
        }
    }*/
}