#pragma once

#include <vector>
#include "TFieldPoint.h"

namespace WorldSide
{
    enum Type
    {
        Nord = 0,
        NordEast,
        East,
        SouthEast,
        South,
        SouthWest,
        West,
        NordWest
    };

    static const Type All[] = { Nord, NordEast, East, SouthEast, South, SouthWest, West, NordWest };
}

template <typename ValueType>
class TField
{
public:
    TField(const int w, const int h, const ValueType defVal)
        : m_w(w)
        , m_h(h)
        , m_values()
    {
        m_values.resize(m_w);
        for (int x = 0; x < m_w; ++x)
        {
            m_values[x].reserve(m_h);
            for (int y = 0; y < m_h; ++y)
                m_values[x].emplace_back(defVal, x, y);
        }
    }

    virtual ~TField()
    {
    }

    /*void SetValue(const int x, const int y, const ValueType val)
    {
        m_values[x][y].m_val = val;
    }

    ValueType GetValue(const int x, const int y) const
    {
        return m_values[x][y].m_val;
    }*/

    TFieldPoint<ValueType>* GetPoint(const int x, const int y)
    {
        if (HasPoint(x, y))
            return &m_values[x][y];

        return nullptr;
    }

    int GetWidth() const { return m_w; }
    int GetHeight() const { return m_h; }
    int GetMinX() const { return 0; }
    int GetMaxX() const { return GetWidth() - 1; }
    int GetMinY() const { return 0; }
    int GetMaxY() const { return GetHeight() - 1; }

    bool HasPoint(const int x, const int y) const
    {
        return x >= GetMinX() &&
            x <= GetMaxX() &&
            y >= GetMinY() &&
            y <= GetMaxY();
    }

private:
    int m_w;
    int m_h;
    std::vector<std::vector<TFieldPoint<ValueType>>> m_values;
};

template <typename ValueType>
TFieldPoint<ValueType>* GetPointNeighbor(TField<ValueType>* field, const TFieldPoint<ValueType>* point, const WorldSide::Type wSide)
{
    if (field == nullptr || point == nullptr)
        return nullptr;

    int newX = point->m_x;
    int newY = point->m_y;

    switch (wSide)
    {
    case WorldSide::Nord:
        --newX;
        break;
    case WorldSide::NordEast:
        --newX;
        ++newY;
        break;
    case WorldSide::East:
        ++newY;
        break;
    case WorldSide::SouthEast:
        ++newX;
        ++newY;
        break;
    case WorldSide::South:
        ++newX;
        break;
    case WorldSide::SouthWest:
        ++newX;
        --newY;
        break;
    case WorldSide::West:
        --newY;
        break;
    case WorldSide::NordWest:
        ++newX;
        --newY;
        break;
    }

    return field->GetPoint(newX, newY);
}