#pragma once

#include <string>
#include <memory>

class Image
{
public:
    Image(char const* data, int const width, int const height)
    {
        if (width <= 0 || height <= 0)
        {
            return;
        }

        int const size = width * height * 3;
        m_data = new char[size];
        std::copy(data, data + size, m_data);
        m_width = width;
        m_height = height;
    }

    ~Image()
    {
        if (m_data != nullptr)
        {
            delete[] m_data;
        }
    }

    char* const GetData() const { return m_data; }
    int const GetWidth() const { return m_width; }
    int const GetHeight() const { return m_height; }

private:
    char* m_data = nullptr;
    int m_width = 0;
    int m_height = 0;
};

class ImageLoader
{
public:
    ImageLoader();
    ~ImageLoader();

    static std::unique_ptr<Image> LoadImageBMP(const std::string& fileName);
    static std::unique_ptr<Image> LoadImagePNG(const std::string& fileName);
};

