#pragma once

const int SCREEN_WIDTH = 600;
const int SCREEN_HEIGHT = 400;

const int SCREEN_TOP = 400;
const int SCREEN_LEFT = 200;

const int CELL_WIDTH = 50;
const int CELL_HEIGHT = 50;

const int MAP_WIDTH = 100;
const int MAP_HEIGHT = 100;