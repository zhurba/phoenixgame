#pragma once

#include <vector>
#include <memory>
#include "Pawn.h"

class PawnManager
{
public:
    ~PawnManager();
    static PawnManager& Instance();

    void Draw() const;
    void AddPawn();
    void DeletePawn();
    void SelectPawn(int x, int y);
    void SetAimToActivePawn(int const aimPosX, int const aimPosY);
    void MovePawns() const;

private:
    PawnManager();
    PawnManager(PawnManager& other) = delete;
    PawnManager(PawnManager&& other) = delete;
    PawnManager& operator=(PawnManager& other) = delete;

private:
    std::vector<std::shared_ptr<Pawn>> m_pawns;
    std::shared_ptr<Pawn> m_activePawn;
};

