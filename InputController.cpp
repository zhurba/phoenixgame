#include "InputController.h"
#include "Constants.h"
#include "PawnManager.h"
#include "MapManager.h"

InputController::~InputController()
{
}

InputController& InputController::Instance()
{
    static InputController s_instance;
    return s_instance;
}
void InputController::Initialize() const
{
    //MapManager::Instance().Initialize();

    //set clear color:
    glClearColor(0.0, 0.5, 0.0, 1.0);

    //set projection:
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0.0, SCREEN_WIDTH, SCREEN_HEIGHT, 0.0);
}

void InputController::OnDraw() const
{
    glClear(GL_COLOR_BUFFER_BIT);

    PawnManager::Instance().Draw();

    glutSwapBuffers();
}

void InputController::OnUpdate() const
{
    PawnManager::Instance().MovePawns();
}

void InputController::OnKeyPress(unsigned char key, int x, int y) const
{
    PawnManager::Instance().AddPawn();
}

void InputController::OnMousePress(int button, int state, int x, int y) const
{
    if (state == GLUT_UP)
    {
        int cellX = x / CELL_WIDTH;
        int cellY = y / CELL_HEIGHT;
        if (button == GLUT_LEFT_BUTTON)
        {
            PawnManager::Instance().SelectPawn(cellX, cellY);
        }
        else if (button == GLUT_RIGHT_BUTTON)
        {
            PawnManager::Instance().SetAimToActivePawn(cellX, cellY);
        }
    }
}

InputController::InputController()
{
}