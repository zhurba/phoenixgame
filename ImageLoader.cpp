#include <fstream>
#include <sstream>
#include <iostream>

#include "ImageLoader.h"
#include "BitStream.h"

ImageLoader::ImageLoader()
{
}

ImageLoader::~ImageLoader()
{
}

// Loader for .bmp images
std::unique_ptr<Image> ImageLoader::LoadImageBMP(const std::string& fileName)
{
    std::ifstream ifs(fileName);
    std::stringstream ss;
    if (ifs.is_open())
    {
        ss << ifs.rdbuf();
        ifs.close();
    }

    if (ss.eof())
    {
        printf("Cannot open file %s\n", fileName.c_str());
        return nullptr;
    }

    unsigned int const headerSize = 54;
    char header[headerSize]; // header
    unsigned int dataPos; // position of data in file
    unsigned int width, height;
    unsigned int imageSize; // size = width * height * 3
    char* data; // RGB-data

    ss.read(header, headerSize);
    if (ss.eof())
    {
        printf("Incorrect .bmp file\n");
        return nullptr;
    }

    if (header[0] != 'B' || header[1] != 'M')
    {
        printf("Incorrect header of .bmp file\n");
        return nullptr;
    }

    dataPos = *(int*) & (header[0x0A]); // data position
    imageSize = *(int*) & (header[0x22]); // size position
    width = *(int*) & (header[0x12]); // width position
    height = *(int*) & (header[0x16]); // height position

    unsigned int pixelCount = width * height;
    if (imageSize == 0)
    {
        imageSize = pixelCount * 3;
    }

    if (dataPos == 0)
    {
        dataPos = headerSize;
    }

    data = new char[imageSize];
    ss.read(data, imageSize);

    //swap R and B components:
    char tmp = 0;
    unsigned int rPos = 0;
    for (unsigned int i = 0; i < pixelCount; ++i)
    {
        rPos = 3 * i;
        tmp = data[rPos];
        data[rPos] = data[rPos + 2];
        data[rPos + 2] = tmp;
    }

    auto image = std::make_unique<Image>(data, width, height);
    delete[] data;

    return image;
}

namespace Convert
{
    union ChunkData
    {
        char m_array[4];
        unsigned int m_number;
    };

    unsigned int SwapUint(unsigned int ui)
    {
        return ((ui >> 24) & 0x000000ff) |
            ((ui >> 8) & 0x0000ff00) |
            ((ui << 8) & 0x00ff0000) |
            ((ui << 24) & 0xff000000);
    }



    unsigned int PCharToUInt(char* pchar)
    {
        ChunkData chunkData;
        for (int i = 0; i < 4; ++i)
        {
            chunkData.m_array[i] = pchar[i];
        }
        return SwapUint(chunkData.m_number);
    }
}

enum EBlockType
{
    NoCompress = 0, // 00
    FixedHaffmanCode = 1, // 01
    DynamicHaffmanCode = 2 // 10
};

struct FixedHaffmanCodeTable
{
    // todo: create and fill the table from article
};

void ParseChunk(char* chunkType, char* chunkContent, unsigned const int chunkContentSize)
{
    if (strcmp(chunkType, "IHDR") == 0)
    {
        unsigned int width = Convert::PCharToUInt(chunkContent);
        unsigned int height = Convert::PCharToUInt(chunkContent + 4);
        unsigned int bitDepth = *(chunkContent + 8);
        unsigned int colorType = *(chunkContent + 9);
        unsigned int compressMethod = *(chunkContent + 10);
        unsigned int filterMethod = *(chunkContent + 11);
        unsigned int scanMethod = *(chunkContent + 12);

        std::cout << "Width = " << width << std::endl;
        std::cout << "Height = " << height << std::endl;
        std::cout << "Bit depth = " << bitDepth << std::endl;
    }
    else if (strcmp(chunkType, "IDAT") == 0)
    {
        unsigned int compressionInfo = *chunkContent >> 4;
        unsigned int windowSize = pow(2, compressionInfo + 8);
        unsigned int compressionMethod = *chunkContent & 0x0f;
        bool useDeflate = compressionMethod == 8;

        unsigned int compressionLevel = *(chunkContent + 1) >> 6;
        unsigned int presetDict = (*(chunkContent + 1) >> 5) & 0b00000001;
        unsigned int checkBits = *(chunkContent + 1) & 0b00011111;

        // Read blocks:
        BitStream bitStream(chunkContent, chunkContentSize);
        bitStream.PopBitsRight(16); // skip first 2 bytes (CMF and FLG)
        bool isFinalBlock = false;
        while (!isFinalBlock && !bitStream.IsEof())
        {
            isFinalBlock = bitStream.PopBitsRight(1);
            EBlockType blockType = static_cast<EBlockType>(bitStream.PopBitsRight(2));
            unsigned int prefix = bitStream.PopBitsRight(7);
            // if ...
            //   prefix += 1 bit
            // todo: use here the table
            int a = 0;
        }

        int a = 0;
        //std::cout << "Chunk IDAT: " << chunkContent << std::endl;
    }
}

// Loader for .png images
std::unique_ptr<Image> ImageLoader::LoadImagePNG(const std::string& fileName)
{
    std::ifstream ifs(fileName, std::ios::binary);
    if (!ifs.is_open())
    {
        printf("Cannot open file %s\n", fileName.c_str());
        return nullptr;
    }

    unsigned const int headerSize = 8;
    char header[headerSize] = "";
    ifs.read(header, headerSize);

    if (header[1] != 'P' || header[2] != 'N' || header[3] != 'G')
    {
        printf("Incorrect header of .png file\n");
        ifs.close();
        return nullptr;
    }

    ifs.seekg(0, ifs.end);
    const int lengthOfFile = ifs.tellg();
    ifs.seekg(headerSize, ifs.beg);

    /*char* buffer = new char[lengthOfFile];
    ifs.read(buffer, lengthOfFile);
    std::cout << buffer;*/

    unsigned int pos = headerSize;
    unsigned int chunkSize = 0;
    unsigned const int chunkLengthSize = 4;
    unsigned const int chunkTypeSize = 4;
    unsigned int chunkContentSize = 0;
    unsigned const int chunkCRCSize = 4;
    char chunkLength[chunkLengthSize] = "";
    char chunkType[chunkTypeSize + 1] = "";
    char* chunkContent = nullptr;
    char chunkCRC[chunkCRCSize] = "";
    Convert::ChunkData chunkData;
    while (!ifs.eof() && pos < lengthOfFile)
    {   
        ifs.read(chunkData.m_array, chunkLengthSize);

        ifs.read(chunkType, chunkTypeSize);
        std::cout << chunkType << std::endl;

        // Convert big-endian to little-endian:
        chunkContentSize = Convert::SwapUint(chunkData.m_number);
        chunkContent = new char[chunkContentSize];
        ifs.read(chunkContent, chunkContentSize);
        if (chunkContent)
        {
            ParseChunk(chunkType, chunkContent, chunkContentSize);
            delete[] chunkContent;
        }

        ifs.read(chunkCRC, chunkCRCSize);

        chunkSize = chunkLengthSize + chunkTypeSize + chunkContentSize + chunkCRCSize;
        pos += chunkSize;
    }

    ifs.close();

    return nullptr;
}