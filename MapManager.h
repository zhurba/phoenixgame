#pragma once

#include <memory>
#include "TField.h"
#include "Notifier.h"

class Pawn;

class MapManager
{
public:
    ~MapManager();
    static MapManager& Instance();

    //void Initialize();
    //std::shared_ptr<TField<std::weak_ptr<Pawn>>> GetField() const;
    void PlacePawn(std::shared_ptr<Pawn> const& pawn);
    void FindPath(std::shared_ptr<Pawn>& pawn, int const endPosX, int const endPosY);
    void UpdatePawnPosition(Pawn const* pawn, int oldX, int oldY);

    void OnPawnPosSet(SenderParamsBase const* paramsBase);

private:
    MapManager();
    MapManager(MapManager& other) = delete;
    MapManager(MapManager&& other) = delete;
    MapManager& operator=(MapManager& other) = delete;

public:
    std::shared_ptr<Receiver<MapManager>> m_receiverPawnPosSet;

private:
    TField<std::weak_ptr<Pawn>> m_field;
};

