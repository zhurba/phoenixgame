#pragma once

#include <list>

#include "TField.h"
#include "TPriorityQueue.h"

template <typename ValueType>
class TPathfinder
{
public:
    TPathfinder() {}
    ~TPathfinder() {}

    static bool FindPath(TField<ValueType>& field, std::list<TFieldPoint<ValueType>*>& path, const int xStart, const int yStart, const int xEnd, const int yEnd);
    static int SqDistance(const TFieldPoint<ValueType>* start, const TFieldPoint<ValueType>* end);
};

template <typename ValueType>
bool TPathfinder<ValueType>::FindPath(TField<ValueType>& field, std::list<TFieldPoint<ValueType>*>& path, const int xStart, const int yStart, const int xEnd, const int yEnd)
{
    TFieldPoint<ValueType>* localStart = field.GetPoint(xStart, yStart);
    TFieldPoint<ValueType>* localEnd = field.GetPoint(xEnd, yEnd);

    std::set<TFieldPoint<ValueType>*> affectedPoints;
    TPriorityQueue<TFieldPoint<ValueType>*, typename TFieldPoint<ValueType>::FPCompareByLength> openPoints;
    openPoints.push(localStart);

    std::set<TFieldPoint<ValueType>*> closedPoints;

    bool status = false;
    while (!openPoints.empty())
    {
        TFieldPoint<ValueType>* current = openPoints.pop();

        if (*current == *localEnd)
        {
            localEnd = current;
            status = true;
            break;
        }

        for (const auto side : WorldSide::All)
        {
            TFieldPoint<ValueType>* neighbor = GetPointNeighbor<ValueType>(&field, current, side);
            if (neighbor)
            {
                if (neighbor->IsOccupied())
                    continue;

                if (closedPoints.find(neighbor) != closedPoints.end())
                    continue;

                int newPathLength = current->m_pathLength + 1;
                TFieldPoint<ValueType>* fpExisting = openPoints.find(neighbor);
                if (fpExisting && fpExisting->m_pathLength < newPathLength)
                    continue;

                if (fpExisting)
                    openPoints.erase(fpExisting);

                neighbor->m_pathLength = newPathLength;
                neighbor->m_prev = current;
                neighbor->m_estLength = SqDistance(neighbor, localEnd);
                openPoints.push(neighbor);
                affectedPoints.insert(neighbor);
            }
        }

        closedPoints.insert(current);
    }

    path.clear();
    if (status)
    {
        path.push_front(localEnd);
        TFieldPoint<ValueType>* fp = localEnd->m_prev;
        while (fp->m_prev != fp)
        {
            path.push_front(fp);
            // field.SetValue(fp->m_x, fp->m_y, 'p');

            //std::cout << fp->m_x << " " << fp->m_y << std::endl;
            fp = fp->m_prev;
        }
        path.push_front(localStart);
    }

    for (auto point : affectedPoints)
    {
        point->Init();
    }

    return status;
}

template <typename ValueType>
int TPathfinder<ValueType>::SqDistance(const TFieldPoint<ValueType>* start, const TFieldPoint<ValueType>* end)
{
    int dx = start->m_x - end->m_x;
    int dy = start->m_y - end->m_y;
    return dx * dx + dy * dy;
}
