#pragma once
class InputController
{
public:
    ~InputController();
    static InputController& Instance();

    void Initialize() const;
    void OnDraw() const;
    void OnUpdate() const;
    void OnKeyPress(unsigned char key, int x, int y) const;
    void OnMousePress(int button, int state, int x, int y) const;

private:
    InputController();
    InputController(InputController& other) = delete;
    InputController(InputController&& other) = delete;
    InputController& operator=(InputController& other) = delete;
};

