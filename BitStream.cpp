#include <cstdio>

#include "BitStream.h"



BitStream::BitStream(const char* buffer, const unsigned int length)
    : m_buffer(nullptr)
    , m_length(0)
    , m_pos(0)
    , m_bitPos(0)
{
    if (buffer && length > 0)
    {
        m_length = length;
        m_buffer = new char[length];
        for (unsigned int i = 0; i < length; ++i)
        {
            m_buffer[i] = buffer[i];
        }
    }
}

BitStream::~BitStream()
{
    if (m_buffer)
    {
        delete[] m_buffer;
    }
}

bool BitStream::IsEmpty()
{
    return !(m_buffer && m_length);
}

bool BitStream::IsEof()
{
    return IsEmpty() || (m_pos == m_length);
}

void BitStream::PopBitsRight(unsigned int number, unsigned int& popValue)
{
    for (unsigned int i = 0; i < number; ++i)
    {
        popValue = (popValue << 1) | (*GetBuffer() & 1);
        *GetBuffer() = *GetBuffer() >> 1;

        ++m_bitPos;
        if (m_bitPos == 8)
        {
            m_bitPos = 0;
            ++m_pos;
        }
        if (m_pos == m_length)
        {
            printf("Reached end of BitStream.\n");
            return;
        }
    }
}

unsigned int BitStream::PopBitsRight(unsigned int number)
{
    unsigned int popValue = 0;
    PopBitsRight(number, popValue);
    return popValue;
}

char* BitStream::GetBuffer()
{
    return m_buffer + m_pos;
}