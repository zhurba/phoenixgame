#pragma once

#include <set>

template <typename ItemType, typename LessType>
class TPriorityQueue
{
public:
    TPriorityQueue() {}
    ~TPriorityQueue() {}

    void push(ItemType member)
    {
        m_set.insert(member);
    }

    ItemType pop()
    {
        auto iter = m_set.begin();
        ItemType item = *iter;
        m_set.erase(*iter);
        return item;
    }

    bool empty() { return m_set.empty(); }

    ItemType find(ItemType member)
    {
        for (const auto& item : m_set)
        {
            if (*item == *member)
                return item;
        }
        return nullptr;
    }

    void erase(ItemType member)
    {
        m_set.erase(member);
    }

private:
    std::set<ItemType, LessType> m_set;
};