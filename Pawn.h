#pragma once

#include <GL/freeglut.h>
#include <array>
#include "Notifier.h"

class Pawn
{
public:
    Pawn();
    virtual ~Pawn();

    void Initialize();
    
    void Draw() const;

    void SetColor(GLfloat r, GLfloat g, GLfloat b);
    void SetSize(int w, int h);

    void SetPos(int x, int y);
    int GetPosX() const;
    int GetPosY() const;

    void SetAimPos(int x, int y);
    int GetAimPosX() const;
    int GetAimPosY() const;

    void SetPath(const std::list<std::array<int, 2>>& path) { m_path = path; }

    void Move();

private:
    void NextStep();

public:
    Sender m_senderPosSet;

private:
    GLuint m_textureId;
    std::array<GLfloat, 3> m_color;
    std::array<int, 2> m_size;
    std::array<int, 2> m_pos;
    std::array<int, 2> m_nextPos;
    std::array<int, 2> m_aimPos;
    GLfloat m_moveRate;
    std::list<std::array<int, 2>> m_path;
};

